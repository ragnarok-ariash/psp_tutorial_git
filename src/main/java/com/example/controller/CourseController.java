package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.model.CourseModel;
import com.example.service.CourseService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class CourseController {
	@Autowired
	CourseService courseDAO;

	@RequestMapping("/course/view")
	public String view(Model model, @RequestParam(value = "courseId", required = false) String courseId) {
		CourseModel course = courseDAO.selectCourse(courseId);

		if (course != null) {
			model.addAttribute("course", course);
			return "viewcourse";
		} else {
			model.addAttribute("courseId", courseId);
			return "not-found-course";
		}
	}

	@RequestMapping("/course/view/{id}")
	public String viewPath(Model model, @PathVariable(value = "id") String courseId) {
		CourseModel course = courseDAO.selectCourse(courseId);

		if (course != null) {
			model.addAttribute("course", course);
			return "viewcourse";
		} else {
			model.addAttribute("courseId", courseId);
			return "not-found-course";
		}
	}

	@RequestMapping("/course/viewall")
	public String view(Model model) {
		List<CourseModel> courses = courseDAO.selectAllCourses();
		model.addAttribute("courses", courses);

		return "viewallcourse";
	}

}
